import React from 'react'
import { StyleSheet, Text, View, SafeAreaView, Image } from 'react-native'
import tw from 'tailwind-react-native-classnames'
import NavOptions from '../components/NavOptions'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { GOOGLE_MAPS_APIKEY } from "@env";

const HomeScreen = () => {

     return (
       <SafeAreaView style={tw `bg-white h-full`}>
       <View style={tw `p-5`}>
         <Image 
           style={{
               width: 100, 
               height: 100, 
               resizeMode: 'contain',
           }}
           source={{
             uri: "https://scontent.fbog2-3.fna.fbcdn.net/v/t1.6435-9/66134989_634216313727785_4135504016522084352_n.png?_nc_cat=102&ccb=1-5&_nc_sid=09cbfe&_nc_eui2=AeEsv4-ksbZLqi49W6qzLwdKQq_gkH014qdCr-CQfTXipwCjHf59s_2icWgv8aZEyxY&_nc_ohc=2ip7T9UToO0AX-CAlRG&_nc_ht=scontent.fbog2-3.fna&oh=37ae88493ea49bb70389f84839f58320&oe=61C334EA",
           }}
         />

         <GooglePlacesAutocomplete 
           placeholder="Where From?"
           styles={{
               container: {
                   flex: 0,
               },
               textInput:{
                  fontSize: 18,
               },
           }}
           query={{
              key: GOOGLE_MAPS_APIKEY,
              language: "en",

           }}
           nearbyPlacesAPI="GooglePlacesSearch"
           debounce={400}


         />
         <NavOptions />
       </View>
       </SafeAreaView>

     );
};

export default HomeScreen;
const styles = StyleSheet.create({

      text: {
              color: 'blue',

      }

});



